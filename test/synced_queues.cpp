#include "blocking_queue.hpp"

#include <iostream>
#include <assert.h>

BlockingQueue<int> audio_queue(4);
BlockingQueue<int> video_queue(4);

int frame_number = 0;
int frame_count = 10;

void display_frames() {
    // Consumer: limited by display timming
    while (frame_number < frame_count) {
        std::cout << "consumed video frame: " << video_queue.front();
        std::cout << " consumed audio frame: " << audio_queue.front() << std::endl;
        assert(video_queue.front() == audio_queue.front());
        video_queue.pop();
        audio_queue.pop();
        // 1 frame per second
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        frame_number++;
    }
    std::cout << "All frames displayed" << std::endl;
}

void read_frame() {
    // Producer: fast as possible
    for (int i = 0; i < frame_count * 2; i++) {
        if (i % 2 == 0) {
            video_queue.push(i / 2);
            std::cout << "produced video frame: " << i / 2 << std::endl;
        }
        else {
            audio_queue.push(i / 2);
            std::cout << "produced audio frame: " << i / 2 << std::endl;
        }
    }
}

int main() {

    std::thread reader_thread = std::thread(read_frame);
    std::thread displayer_thread = std::thread(display_frames);


    reader_thread.join();
    displayer_thread.join();

    return EXIT_SUCCESS;
}