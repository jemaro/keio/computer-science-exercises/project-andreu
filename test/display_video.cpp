#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Widget.H>

#include <iostream>

#include "Fl_ViewerCV.h"
#include "test_utils.hpp"
#include "AV_input.hpp"

Fl_Window* window;
Fl_ViewerCV* video_viewer;
cv::VideoCapture cap;
double FPS_1;

void display_frame(void*) {
    cv::Mat frame;
    cap >> frame;
    if (frame.empty()) {
        cap.release();
        Fl::delete_widget(window);
    }
    else {
        video_viewer->SetImage(&frame);
        video_viewer->UpdateView();
        Fl::repeat_timeout(FPS_1, display_frame);
    }
}

int main(int argc, char* argv[]) {
    std::string filename = get_video_filename(argc, argv);

    window = new Fl_Window(600, 600, "Display Video");
    video_viewer = new Fl_ViewerCV(0, 0, 600, 600);
    window->end();
    window->resizable(video_viewer);

    AV_input input = AV_input(filename);
    cap = input.video->capture;
    FPS_1 = 1 / cap.get(cv::CAP_PROP_FPS);
    window->show();

    // Main loop
    Fl::add_timeout(FPS_1, display_frame);
    Fl::run();
    return 0;
}