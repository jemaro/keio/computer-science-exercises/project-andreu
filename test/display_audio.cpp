// If not ordered like this, the compiler will throw an error. Display X11
// conflicts
#include <opencv2/highgui/highgui.hpp>

#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/Fl_Window.H>

#include <iostream>
#include <numeric>

#include "AV_input.hpp"
#include "Fl_ViewerCV.h"
#include "processors.hpp"
#include "test_utils.hpp"


int main(int argc, char* argv[]) {
    std::string filename = get_video_filename(argc, argv);

    // Initialize FLTK window
    Fl_Window* window         = new Fl_Window(600, 600, "Display Audio");
    Fl_ViewerCV* audio_viewer = new Fl_ViewerCV(0, 0, 600, 600);
    window->end();
    window->resizable(audio_viewer);

    // Initialize audio buffers data
    AV_input input = AV_input(filename);

    // Initialize OpenCV image
    cv::Mat mat;
    int av_frames = 0;

    // Read audio loop
    while (input.ReadFrame() >= 0) {
        if (input.current_packet_is_audio()) {
            if (input.audio->DecodePacket() >= 0) {
                // AVFrame* fra = av_frame_alloc();
                int ret = swr_convert(input.audio->resample_context,
                    // Output buffer and number of samples
                    input.frame->data, input.frame->nb_samples,
                    // Input buffer and number of samples
                    (const uint8_t**)input.frame->data,
                    input.frame->nb_samples);
                if (ret < 0) {
                    std::cout << AVERROR_OUTPUT_CHANGED << std::endl;
                    std::cout << AVERROR_INPUT_CHANGED << std::endl;
                }
                mat = process::audio::plot(
                    &input.frame->data[0][0], input.frame->nb_samples);
                cv::imshow("Display audio", mat);
                cv::waitKey(10);
            }
        } else if (input.current_packet_is_video()) {
            ++av_frames;
        }
    }
    std::cout << input.video->capture.get(cv::CAP_PROP_FRAME_COUNT)
              << std::endl;
    std::cout << av_frames << std::endl;
}