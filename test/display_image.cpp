#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Widget.H>

#include "Fl_ViewerCV.h"
#include "test_utils.hpp"

Fl_Window* window;

void timeout_callback(void*) {
    Fl::delete_widget(window);
}

int main(int argc, char* argv[]) {
    std::string filename = get_image_filename(argc, argv);

    window = new Fl_Window(600, 600, "Display Image");
    Fl_ViewerCV* video_viewer = new Fl_ViewerCV(0, 0, 600, 600);
    window->end();
    window->resizable(video_viewer);
    cv::Mat mat = cv::imread(filename);
    video_viewer->SetImage(&mat);
    video_viewer->UpdateView();
    window->show();

    Fl::add_timeout(1, timeout_callback);
    Fl::run();

    return 0;
}