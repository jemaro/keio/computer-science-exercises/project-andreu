#include <iostream>

#include "app.hpp"
#include "test_utils.hpp"

App app = App();

void SlideToEnd(void* userdata) {
    App* app = static_cast<App*>(userdata);
    app->ui.frame_slider->value(159);
    app->ui.frame_slider->do_callback();
}

int main(int argc, char* argv[]) {
    std::string filename = get_video_filename(argc, argv);
    app.Load(filename);
    app.Play();

    // Fl::add_timeout(2, SlideToEnd, &app);

    // This will block until the user closes the window
    return Fl::run();
}