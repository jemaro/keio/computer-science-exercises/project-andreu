// If not ordered like this, the compiler will throw an error. Display X11
// conflicts
#include <opencv2/highgui/highgui.hpp>

#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/Fl_Window.H>

#include <iostream>
#include <numeric>

#include "test_utils.hpp"
#include "Fl_ViewerCV.h"
#include "AV_input.hpp"
#include "processors.hpp"
#include "blocking_queue.hpp"

Fl_Window* window;
Fl_ViewerCV* video_viewer;
Fl_ViewerCV* audio_viewer;
AV_input input;
std::atomic<bool> is_playing;

int next_frame = 0;

// Initialize FLTK window
void CreateWindow() {
    window       = new Fl_Window(600, 600, "Display Audio and video");
    audio_viewer = new Fl_ViewerCV(0, 0, 600, 300);
    video_viewer = new Fl_ViewerCV(0, 300, 600, 300);
    window->end();
    window->resizable(video_viewer);
    window->show();
}

// Consumer of input.audio->images and input.video->images
void display_images(void*) {
    // Extract the images from the queues
    cv::Mat video_img, audio_img;
    std::tie(video_img, audio_img) = input.get_images();
    if (audio_img.empty() || video_img.empty()) {
        // Close window if the images are empty
       Fl::delete_widget(window);
    } else {
        // Display the images
        audio_viewer->SetImage(&audio_img);
        audio_viewer->UpdateView();
        video_viewer->SetImage(&video_img);
        video_viewer->UpdateView();
        Fl::repeat_timeout(input.time_step, display_images);
        std::cout << "displayed frame number: " << next_frame++
                  << std::endl;
    }
}

void go_to_frame_20(void*) {
    std::cout << "TEST: move backward" << std::endl;
    input.StartReading(20);
    next_frame = 20;
}

int main(int argc, char* argv[]) {
    std::string filename = get_video_filename(argc, argv);

    // Initialize FLTK window
    CreateWindow();
    // Initialize input
    input.Load(filename);
    // Initialize the image production pipeline
    input.StartReading();
    // Initialize the frame displayer
    Fl::add_timeout(input.time_step, display_images);
    // Wait one second and move to another frame
    Fl::add_timeout(4, go_to_frame_20);
    // Main loop
    Fl::run();

    // Twice to see if it gets stuck or the reader exits gratefully
    std::cout << "TEST: Start from end of file" << std::endl;
    CreateWindow();
    input.StartReading();
    Fl::add_timeout(input.time_step, display_images);
    Fl::run();

    return EXIT_SUCCESS;
}