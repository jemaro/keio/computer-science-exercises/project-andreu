// If not ordered like this, the compiler will throw an error. Display X11
// conflicts
#include <opencv2/highgui/highgui.hpp>

#include <iostream>
#include <numeric>

#include "AV_input.hpp"
#include "processors.hpp"
#include "test_utils.hpp"

int main(int argc, char* argv[]) {
    std::string filename = get_video_filename(argc, argv);

    // Initialize audio buffers data
    AV_input input = AV_input(filename);

    // Read audio loop
    while (input.ReadFrame() >= 0) {
        if (input.current_packet_is_audio()) {
            if (input.audio->DecodePacket() >= 0) {
                // std::cout << input.frame->sample_rate << std::endl;
                AVFrame* frame = input.frame;
                std::vector<uint8_t> audio_data;
                audio_data.insert(audio_data.end(), frame->extended_data[0],
                    frame->extended_data[0] + frame->nb_samples);
                cv::Mat audio_img = process::audio::mfcc(&audio_data);
                cv::imshow("Display audio", audio_img);
                cv::waitKey(10);
            }
        }
    }
}
