#include "AV_input.hpp"
#include "processors.hpp"
#include "test_utils.hpp"

#include <iostream>
#include <queue>

AV_input input;
std::queue<cv::Mat> video_img_queue;
std::queue<cv::Mat> audio_img_queue;
std::queue<AVFrame*> audio_queue;
int current_frame_number = 0;

// Reads x frames into the queues
void ReadFrames(int x) {
    while (input.ReadFrame() >= 0) {
        if (input.current_packet_is_audio()) {
            if (input.audio->DecodePacket() >= 0) {
                AVFrame* audio_frame = av_frame_clone(input.frame);
                audio_queue.push(audio_frame);
            }
        } else if (input.current_packet_is_video()) {
            cv::Mat video_img;
            input.video->capture >> video_img;
            video_img_queue.push(video_img);
        }
        if (video_img_queue.size() >= static_cast<long unsigned int>(x)) {
            break;
        }
    }

    std::cout << "video buffer: " << video_img_queue.size() << std::endl;
    std::cout << "audio buffer: " << audio_queue.size() << std::endl;

    if (audio_queue.empty()) {
        std::cout << "audio queue is empty" << std::endl;
    }

    // Produce images from the audio data
    int64_t lower_pts = 0;
    while (!audio_queue.empty()) {
        std::cout << "audio queue size: " << audio_queue.size() << std::endl;
        // Image presentation timestamp limit
        int64_t upper_pts =
            input.audio->ImgNum2Pts(current_frame_number + audio_img_queue.size() + 1);
        std::cout << "limit_pts: " << upper_pts << std::endl;

        // Extract audio data from the audio frames that correspond to the
        // video frame
        std::vector<uint8_t> audio_data;
        while (audio_queue.front()->pts < upper_pts) {
            if (audio_queue.front()->pts < lower_pts) continue;
            audio_data.insert(audio_data.end(),
                audio_queue.front()->extended_data[0],
                audio_queue.front()->extended_data[0]
                    + audio_queue.front()->nb_samples);
            audio_queue.pop();
            if (audio_queue.empty()) {
                break;
            }
        }
        if (!audio_data.empty()) {
            // Process audio data to create an image
            audio_img_queue.push(process::audio::signal(&audio_data));
            lower_pts = upper_pts;
        }
    }
    std::cout << "Finished" << std::endl;
}

int main(int argc, char* argv[]) {
    int objective_frame_number = 52;
    std::string filename       = get_video_filename(argc, argv);
    input.Load(filename);

    // Read frames
    ReadFrames(objective_frame_number + 10);

    // Empty queues and save the second frame in independent variables
    cv::Mat video_img_x, audio_img_x;
    int video_img_number = 0;
    while (!video_img_queue.empty()) {
        if (video_img_number++ == objective_frame_number) {
            video_img_x = video_img_queue.front();
        }
        video_img_queue.pop();
    }
    int audio_img_number = 0;
    while (!audio_img_queue.empty()) {
        if (audio_img_number++ == objective_frame_number) {
            audio_img_x = audio_img_queue.front();
        }
        audio_img_queue.pop();
    }

    // Seek the frame and read some frames again
    std::cout << "Seeking to frame " << std::endl;
    current_frame_number = objective_frame_number;
    input.SeekFrame(objective_frame_number);

    // Read frames again
    ReadFrames(20);

    // The first frame in the queue should be the second of the file

    // TODO
    // cv::cvtColor(video_img_x, video_img_x, cv::COLOR_BGR2GRAY);
    // cv::cvtColor(video_img_queue.front(), video_img_queue.front(),
    // cv::COLOR_BGR2GRAY); cv::Mat diff; cv::compare(video_img_x,
    // video_img_queue.front(), diff, cv::CMP_EQ); std::cout <<
    // cv::countNonZero(diff) << std::endl;

    ShowManyImages("Image", 4, video_img_x, video_img_queue.front(),
        audio_img_x, audio_img_queue.front());

    // TODO reload and check seeking forward

    return EXIT_SUCCESS;
}