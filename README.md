# Andreu Giménez Bolinches

## Abstract

Cross platform User Interface written in C++ for visualizing and analyzing
video images and sound. 

This project does not intend to build an innovative tool, as video editors
already have most of the features I plan to add. The expected outcome is an
improvement of my C++ skills in medium sized projects; an introduction to OS
independent User Interfaces able to use OS components like the file explorer;
and experience with showing intermediate results in a User Interface.

The outcome of this project will not be used directly in my master thesis
research but I intend to select and get familiar with a UI framework that I can
use in my master thesis as well as some debugging and visualization processes.

## Progress Report

Being it a C++ learning project I focused on developing the different features
in a modular fashion, understanding each of them separately instead of trying
to dive in a big codebase from the first moment.

As GUI framework, I have decided to use [FLTK](https://www.fltk.org/) as it is
lightweight and cross-platform (_Fast Light ToolKit_). Additionally it supports
3D graphics via [OpenGL](https://www.opengl.org/). Although 3D graphics won't
be used in this project.

[OpenCV](https://www.opencv-srf.com/p/introduction.html) will be used to
process input images and [FFmpeg](https://ffmpeg.org/doxygen/trunk/index.html)
libraries will be used for audio processing and image and audio
synchronization. 

### Repository

The [code repository ][repo] is publicly stored in
[*Gitlab*](https://gitlab.com/) integrated with [`git`](https://git-scm.com/)
version control.

Apart from the main code, there are [extra scripts ][extras] that allow anybody
using Ubuntu or Windows Subsystem for Linux to setup the necessary third party
libraries.

### Build environment

C++ is a compiled language, and therefore it needs to be built into executables
to be used. The build system is based on [CMake](https://cmake.org/), which at
the same time uses [make](https://www.gnu.org/software/make/) and
[gcc](https://gcc.gnu.org/). My developing environment is based on [Ubuntu
20.02 LTS](https://releases.ubuntu.com/20.04/) but I use it inside [Windows
Subsystem for
Linux](https://www.microsoft.com/en-us/p/ubuntu-2004-lts/9n6svws3rx71) which
makes my developer environment easily replicable by any other software
developer as [the two most operating system distributions for software
development worldwide are Linux and Windows
](https://www.statista.com/statistics/869211/worldwide-software-development-operating-system/).

[`CMakeLists.txt`][CMakeLists] is the main build file, which configures the
libraries the source files used and the libraries required. Being unexperienced
with `CMake` I have spent a significative amount of time debugging issues at
build time. Additionally I use
[`CTest`](https://gitlab.kitware.com/cmake/community/-/wikis/doc/ctest/Testing-With-CTest)
to test each feature separately. [`CMakeLists.txt`][CMakeLists] contains a
little test discovery device that will compile any `.cpp` file in the `test`
folder with the required libraries and flag them as tests. I plan to extend it
in the future to test the different features with a variety of video and audio
formats instead of just one as it is right now.

### OpenCV viewer in FLTK user interface

[`Fl_ViewerCV`][Fl_ViewerCV] is a widget viewer for OpenCV images in `FLTK` user
interfaces. It allows to mimic the built-in `OpenCV` viewer, but inside a
`FLTK` window. Therefore it only needs to be provided a `cv::Mat` image and it
will be displayed. [`display_image.cpp`][display_image] is a simple test of this
feature where a `cv::Mat` image is loaded using `OpenCV`s `cv::imread` and
displayed in a window that contains a [`Fl_ViewerCV`][Fl_ViewerCV] widget.

![OpenCV viewer in FLTK user interface](README_files/display_image.jpg)

### Audiovisual input class

Unfortunately, `OpenCV` only deals with the image part of a video when reading
it, therefore I need to use another third party library to read the audio part
of a video: [`FFmpeg`](https://ffmpeg.org/doxygen/trunk/index.html) is a low
level library that allows to read and write audio and video files. I'm only
interested in reading though. This library could be used to read and process
the images but I'm interested in the high level features that `OpenCV` offers.

Therefore the class [`AV_input`][AV_input] is introduced to mix `OpenCV` video
reading and `FFmpeg` audio reading as a complete audiovisual input.

#### Displaying images from a video

[`display_video.cpp`][display_video] tests this feature by loading a video
using [`AV_input`][AV_input] and display its frames using
[`Fl_ViewerCV`][Fl_ViewerCV] timed with `FLTK` according to the video sampling
rate.

![Audiovisual input class](README_files/display_video.jpg)

#### Displaying audio from a video

[`display_audio.cpp`][display_audio] in contrast uses [`AV_input`][AV_input] to
load a video and its audio. Then the audio is decoded and the signal is plotted
into a `cv::Mat`, which is displayed using [`Fl_ViewerCV`][Fl_ViewerCV].
Everything is processed in real time, the audio is not preprocessed and then
plotted as many `C++` libraries offer, instead is plotted as soon as it is
read.

An utility function `plot` is provided in [`utils.hpp`][utils] that takes as
input a data vector (its pointer and size) together with some optional
configuration parameters and returns a `cv::Mat` image with the data plotted.

![Display audio from a video file](README_files/display_audio.jpg)

## Next steps

+ Audio and image have been displayed separately up to now. It's
  synchronized display has been explored although it is not yet implemented.

+ The main features of this project have been implemented and tested
  separately. However, I still need to bundle everything together as a single
  _application_ window. 

+ Reproduction controls need to be developed and bound to `FLTK` widgets.

+ Tests' configuration should be improved in order to iterate through different
  video and audio formats.

+ Add audio and image processing options: Once the framework for the audio and
  image reading and display is in place, I should be able to add optional
  callbacks that process the audio and image data before it is displayed.

[repo]:
https://gitlab.com/jemaro/keio/computer-science-exercises/project-andreu
[extras]:
https://gitlab.com/jemaro/keio/computer-science-exercises/project-andreu/-/tree/main/extras
[CMakeLists]:
https://gitlab.com/jemaro/keio/computer-science-exercises/project-andreu/-/blob/main/CMakeLists.txt
[AV_input]:
https://gitlab.com/jemaro/keio/computer-science-exercises/project-andreu/-/blob/main/src/AV_input.hpp
[Fl_ViewerCV]:
https://gitlab.com/jemaro/keio/computer-science-exercises/project-andreu/-/blob/main/src/Fl_ViewerCV.h
[display_image]:
https://gitlab.com/jemaro/keio/computer-science-exercises/project-andreu/-/blob/main/test/display_image.cpp
[display_video]:
https://gitlab.com/jemaro/keio/computer-science-exercises/project-andreu/-/blob/main/test/display_video.cpp
[display_audio]:
https://gitlab.com/jemaro/keio/computer-science-exercises/project-andreu/-/blob/main/test/display_audio.cpp
[utils]:
https://gitlab.com/jemaro/keio/computer-science-exercises/project-andreu/-/blob/main/test/utils.hpp