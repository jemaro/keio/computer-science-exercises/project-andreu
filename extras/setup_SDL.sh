#!/bin/bash
# Andreu Gimenez Bolinches esdandreu@gmail.com 16/09/2021

# Tested in:
# WSL Ubuntu 20.04

# The first argument is the number of processes to use on the setup build
# Note: "sudo make install" is used in order to install the package globaly
cd /tmp
git clone https://github.com/libsdl-org/SDL
cd SDL && \
    mkdir -p build && cd build && \
    cmake -D CMAKE_BUILD_TYPE=Release -D BUILD_EXAMPLES=OFF -D BUILD_DOCS=OFF \
        -D BUILD_PERF_TESTS=OFF -D BUILD_TESTS=OFF \
        -D CMAKE_INSTALL_PREFIX=/usr/local .. && \
    make -j$nproc && sudo make install && \
    cd / && sudo rm -rf /tmp/SDL