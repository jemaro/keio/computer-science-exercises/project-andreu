#!/bin/bash
# Andreu Gimenez Bolinches esdandreu@gmail.com 16/09/2021

# Tested in:
# WSL Ubuntu 20.04

# The first argument is the number of processes to use on the setup build
nproc=${1}
version=1.3

# Notes: 
# 
# fltk must be built with the compiler flag -fPIC
# https://stackoverflow.com/questions/19768267/relocation-r-x86-64-32s-against-linking-error

cd /tmp 
git clone https://github.com/fltk/fltk.git
cd fltk && git checkout branch-$version && \
    mkdir -p build && cd build && \
    cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-std=c++11 -fPIC" .. && \
    make -j$nproc && sudo make install && \
    cd / && sudo rm -rf /tmp/fltk