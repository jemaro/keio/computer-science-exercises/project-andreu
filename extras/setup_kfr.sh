sudo apt install clang
cd /tmp 
git clone https://github.com/kfrlib/kfr.git
cd kfr && \
    mkdir -p build && cd build && \
    cmake -DENABLE_TESTS=ON \
        -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_BUILD_TYPE=Release .. && \
    make -j && sudo make install && \
    cd / && sudo rm -rf /tmp/kfr