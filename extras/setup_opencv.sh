#!/bin/bash
# Andreu Gimenez Bolinches esdandreu@gmail.com 16/09/2021

# Tested in:
# WSL Ubuntu 20.04

# The first argument is the number of processes to use on the setup build

# OpenCV ises pkg-config and it can have some trouble finding packages if it is
# not installed.
# https://stackoverflow.com/questions/69029560/when-i-build-opencv-it-does-not-recognise-my-installed-ffmpeg
# sudo apt install pkg-config

sudo apt-get install -y python3-dev python3-numpy python3-matplotlib

# Troubleshoot: if command below throws an error like "E: Unable to correct
# problems, you have held broken packages.", most probably you have tried to
# install the newer version of FFmpeg before installing the older version. Run
# the following commands
# (https://forum.linuxconfig.org/t/ffmpeg-solve-unmet-dependencies/5356): 
# sudo apt-get clean ; sudo apt-get update ; sudo apt-get check ; \
#     sudo apt-get purge ffmpeg* -y ; sudo apt-get autoremove -y ; \ 
#     sudo apt-get -f satisfy ffmpeg -y

sudo apt-get install -y libavcodec-dev libavformat-dev libavutil-dev \
    libswscale-dev libswresample-dev
sudo apt install ffmpeg -y
sudo apt-get install -y libgstreamer-plugins-base1.0-dev libgstreamer1.0-dev
sudo apt-get install -y libgtk2.0-dev

OPENCV_VERSION='4.5.1'

# Note: "sudo make install" is used in order to install the package globaly
cd /tmp
git clone https://github.com/opencv/opencv.git
cd opencv && git checkout $OPENCV_VERSION && \
    mkdir -p build && cd build && \
    cmake -D CMAKE_BUILD_TYPE=Release -D BUILD_EXAMPLES=OFF -D BUILD_DOCS=OFF \
        -D BUILD_PERF_TESTS=OFF -D BUILD_TESTS=OFF -D BUILD_FFMPEG=OFF \
        -D CMAKE_INSTALL_PREFIX=/usr/local .. && \
    make -j$nproc && sudo make install
cd / && sudo rm -rf /tmp/opencv