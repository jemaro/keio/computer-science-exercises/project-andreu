# December 2021: FFmpeg 4.4.1 can only be installed on Ubuntu 20.04 through an
# external package manager. The official ubuntu repository has FFmpeg 4.2.1
sudo apt-get clean ; sudo apt-get update ; sudo apt-get check ; \
    sudo apt-get purge ffmpeg* -y ; sudo apt-get autoremove -y ; \ 
    sudo apt-get -f satisfy ffmpeg -y
sudo apt-get install -y libavcodec-dev libavformat-dev libavutil-dev \
    libavdevice-dev libswscale-dev libswresample-dev libpostproc-dev
sudo apt-get install -y ffmpeg
sudo add-apt-repository -y ppa:savoury1/ffmpeg4
sudo add-apt-repository -y ppa:savoury1/graphics
sudo add-apt-repository -y ppa:savoury1/multimedia
# FFmpeg is already installed so we only need to update the package list
sudo apt install ffmpeg -y
sudo apt -y full-upgrade
sudo add-apt-repository -y --remove ppa:savoury1/ffmpeg4
sudo add-apt-repository -y --remove ppa:savoury1/graphics
sudo add-apt-repository -y --remove ppa:savoury1/multimedia
ffmpeg -version