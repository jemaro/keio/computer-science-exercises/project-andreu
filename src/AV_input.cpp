#include "AV_input.hpp"

AV_input::Stream::Stream(AV_input* parent, enum AVMediaType type) {
    input = parent;
    // Find the stream
    stream_index =
        av_find_best_stream(input->format_context, type, -1, -1, &codec, 0);
    if (stream_index == AVERROR_STREAM_NOT_FOUND || !codec) {
        throw std::runtime_error("Could not find stream");
    }
    // Get the codec context
    codec_context = avcodec_alloc_context3(codec);
    if (!codec_context) {
        throw std::runtime_error("Out of memory");
    }
    // Set the parameters of the codec context from the stream
    int error = avcodec_parameters_to_context(
        codec_context, parent->format_context->streams[stream_index]->codecpar);
    if (error < 0) {
        throw std::runtime_error("Could not set the codec context");
    }
}

void AV_input::Load(const std::string& filename) {
    if (avformat_open_input(&format_context, filename.c_str(), NULL, NULL)
        < 0) {
        throw std::invalid_argument("Could not open audio input");
    }
    // Dump information about the file into standard error
    av_dump_format(format_context, 0, filename.c_str(), 0);

    video = new Video(this, filename);
    audio = new Audio(this);

    av_new_packet(packet, 0);
}

AV_input::~AV_input() {
    StopReading();
    delete video;
    delete audio;
    av_packet_unref(packet);
    av_frame_free(&frame);
    avformat_close_input(&format_context);
};

int AV_input::Stream::DecodePacket() {
    if (avcodec_send_packet(codec_context, input->packet) < 0) {
        throw std::runtime_error("Error sending a packet to the decoder");
    }
    return avcodec_receive_frame(codec_context, input->frame);
}

int AV_input::SeekFrame(int img_num) {
    int audio_success = av_seek_frame(format_context, audio->stream_index,
        audio->ImgNum2Pts(img_num), AVSEEK_FLAG_ANY);
    video->capture.set(cv::CAP_PROP_POS_FRAMES, img_num);
    int video_success = av_seek_frame(format_context, video->stream_index,
        video->ImgNum2Pts(img_num), AVSEEK_FLAG_ANY);
    return audio_success * video_success;
}

void AV_input::Read(AV_input* input) {
    input->is_reading.store(true);
    while (!input->is_stopping.load() && input->ReadFrame() >= 0) {
        // if (input->ReadFrame() < 0) { // End of file
        //     break;
        // }
        if (input->current_packet_is_audio()) {
            if (input->audio->DecodePacket() >= 0) { // Success decoding
                // Push audio frame to queue
                AVFrame* audio_frame = av_frame_clone(input->frame);
                input->audio->frames.push(audio_frame);
#ifdef DEBUG
                std::cout << "++ audio.frames: " << input->audio->frames.size()
                          << std::endl;
#endif
            }
        } else if (input->current_packet_is_video()) {
            // Push video frame to queue
            cv::Mat video_img;
            input->video->capture >> video_img;
            input->video->frames.push(video_img);
#ifdef DEBUG
            std::cout << "++ video.frames: " << input->video->frames.size()
                      << std::endl;
#endif
        }
    }
    input->is_reading.store(false);
    // Send empty frames as stop signal for the process threads
    AVFrame* empty_frame = av_frame_alloc();
    input->video->frames.push(cv::Mat());
    input->audio->frames.push(empty_frame);
#ifdef DEBUG
    std::cout << "Finished reading" << std::endl;
#endif
}

void AV_input::Video::ProcessFrames(AV_input* input) {
    // Consume video data from the queue, process it and push it to the
    // video image queue
    Video* _this = input->video;
    _this->is_processing.store(true);
    while (!input->is_stopping.load()) {
        cv::Mat frame = _this->frames.front();
        _this->frames.pop();
#ifdef DEBUG
        std::cout << "-- video.frames: " << input->video->frames.size()
                  << std::endl;
#endif
        if (frame.empty()) { // Stop signal received
            break;
        }
        cv::Mat video_img = _this->processors[_this->processor_index](&frame);
        _this->images.push(video_img);
#ifdef DEBUG
        std::cout << "++ video.images: " << input->video->images.size()
                  << std::endl;
#endif
    }
    if (!input->is_stopping.load()) { // Stopped by end of file
        _this->images.push(cv::Mat());
    }
    _this->is_processing.store(false);
#ifdef DEBUG
    std::cout << "Finished processing video" << std::endl;
#endif
}

void AV_input::Audio::ProcessFrames(AV_input* input) {
    // Consume audio data from the queue, process it and push it to the
    // audio image queue
    Audio* _this = input->audio;
    _this->is_processing.store(true);
    // Defined the time region of interest (the image duration)
    int64_t lower_pts = _this->ImgNum2Pts(_this->back_image_num());
    int64_t upper_pts = _this->ImgNum2Pts(_this->back_image_num() + 1);
    std::vector<uint8_t> audio_data;
    while (!input->is_stopping.load()) {
        // Check the empty frame signal
        AVFrame* frame = _this->frames.front();
        if (frame->nb_samples == 0) { // Stop signal received
            break;
        }
#ifdef DEBUG
        std::cout << "Producing audio image " << _this->back_image_num()
                  << " pts: [ " << lower_pts << ", " << upper_pts << " ]"
                  << std::endl;
#endif
        if (frame->pts > lower_pts) {
            // Accumulate the audio data in our region of interest
            if (frame->pts < upper_pts) {
                // Resample to uint8_t, override the existing data as it is
                // going to be popped from the queue anyway
                int ret = swr_convert(_this->resample_context,
                    // Output buffer and number of samples
                    frame->extended_data, frame->nb_samples,
                    // Input buffer and number of samples
                    (const uint8_t**)frame->extended_data,
                    frame->nb_samples);
                if (ret < 0) {
                    throw std::runtime_error("Could not resample audio");
                }
                audio_data.insert(audio_data.end(), frame->extended_data[0],
                    frame->extended_data[0] + frame->nb_samples);
                _this->frames.pop();
#ifdef DEBUG
                std::cout << "-- audio.frames: " << _this->frames.size()
                          << std::endl;
#endif
            } else { // Produce an image once we exceed the region of interest
                cv::Mat audio_img;
                if (!audio_data.empty()) {
                    audio_img =
                        _this->processors[_this->processor_index](&audio_data);
                    audio_data.clear();
                }
                _this->images.push(audio_img);
                lower_pts = upper_pts;
                upper_pts = _this->ImgNum2Pts(_this->back_image_num() + 1);
#ifdef DEBUG
                std::cout << "++ audio.images: " << _this->images.size()
                          << std::endl;
#endif
            }
        } else {
            // Discard frames before our region of interest
            _this->frames.pop();
#ifdef DEBUG
            std::cout << "-- (discarded) audio.frames: " << _this->frames.size()
                      << std::endl;
#endif
        }
    }
    if (!input->is_stopping.load()) { // Stopped by end of file
        _this->images.push(cv::Mat());
    }
    _this->is_processing.store(false);
#ifdef DEBUG
    std::cout << "Finished processing audio" << std::endl;
#endif
}

void AV_input::StartReading(int img_num) {
    if (is_reading.load() && !is_stopping.load()) {
        StopReading();
    }
    if (img_num > 0) {
        std::cout << "Seek result " << SeekFrame(img_num) << std::endl;
        video->images.pop_count = img_num;
        audio->images.pop_count = img_num;
    }
    // Start new threads
    read_thread           = std::thread(Read, this);
    video->process_thread = std::thread(video->ProcessFrames, this);
    audio->process_thread = std::thread(audio->ProcessFrames, this);
}

void AV_input::StopReading() {
    // Once the reading is stopped, the reading thread can still produce one
    // frame plus an empty frame to each queue. The process frames can still
    // ask one frame from each queue and produce one image for each queue plus
    // an empty image for each queue.
#ifdef DEBUG
    std::cout << "Stop reading" << std::endl;
#endif
    is_stopping.store(true);
    // Stop the reading thread first by emptying the frames queue
    while (is_reading.load()) {
        video->frames.clear();
        audio->frames.clear();
    }
    if (read_thread.joinable()) {
        read_thread.join();
    }
    // Stop the audio and video processing threads by consuming images until
    // their producers finish
    while (video->is_processing.load() || audio->is_processing.load()) {
        if (video->is_processing.load()) {
            // // if (video->frames.empty()){
            // //     video->frames.push(cv::Mat());
            // // }
            video->images.clear();
        }
        if (audio->is_processing.load()) {
            audio->images.clear();
        }
    }
    if (video->process_thread.joinable()) {
        video->process_thread.join();
    }
    if (audio->process_thread.joinable()) {
        audio->process_thread.join();
    }
    // Ensure all queues are empty
    video->frames.clear();
    video->images.clear();
    audio->frames.clear();
    audio->images.clear();
    is_stopping.store(false);
}

std::tuple<cv::Mat, cv::Mat> AV_input::get_images() {
    // End of file
    // if (video->images.empty() && audio->images.empty()
    //     && !video->is_processing.load() && !audio->is_processing.load()) {
    //     return std::make_tuple(cv::Mat(), cv::Mat());
    // }
    // Extract the images from the queues
    cv::Mat video_img = video->images.front();
    video->images.pop();
    cv::Mat audio_img = audio->images.front();
    audio->images.pop();
#ifdef DEBUG
    std::cout << "-- video.images: " << video->images.size() << std::endl
              << "-- audio.images: " << audio->images.size() << std::endl;
#endif
    if (audio_img.empty() && video_img.empty()) { // End of file
        StopReading(); // Cleanup of threads and queues
    }
    return std::make_tuple(video_img, audio_img);
}