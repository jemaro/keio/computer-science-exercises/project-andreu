#ifndef AV_INPUT_HPP_INCLUDED
#define AV_INPUT_HPP_INCLUDED


#include <opencv2/videoio.hpp>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswresample/swresample.h>
}

#include "blocking_queue.hpp"
#include "processors.hpp"

#ifdef DEBUG
#include <iostream>
#endif
#include <atomic>

// Queue of extracted video and audio frames, they do not need to have the
// same duration
#define AV_INPUT_AUDIO_FRAMES_QUEUE_SIZE 10
#define AV_INPUT_VIDEO_FRAMES_QUEUE_SIZE 1
// Queues of images to be displayed, they should have the same duration
#define AV_INPUT_AUDIO_IMAGES_QUEUE_SIZE 5
#define AV_INPUT_VIDEO_IMAGES_QUEUE_SIZE 5

/**
 * @brief Input audiovisual data
 *
 */
class AV_input {

    struct Stream {
        int stream_index;
        AV_input* input;
        AVCodec* codec                = NULL;
        AVCodecContext* codec_context = NULL;

        /**
         * @brief Construct a new Stream object
         *
         * @param parent Parent audiovisual input, allows access to the packet,
         * the format context, etc.
         * @param type Wether the stream is video or audio
         */
        Stream(AV_input* parent, enum AVMediaType type);

        /**
         * @brief Construct a new empty Stream object
         *
         */
        Stream(){};

        /**
         * @brief Destroy the Stream object freeing the codec context.
         *
         */
        ~Stream() {
            if (codec_context != NULL)
                avcodec_close(codec_context);
        }

        /**
         * @brief Wrapper for avcodec_send_packet and avcodec_receive_frame.
         *
         * @return int
         */
        int DecodePacket();

        /**
         * @brief Converts an image number into a presentation timestamp (time
         * when the frame should be shown to the user) in audio stream time_base
         * units.
         *
         * @return int64_t Presentation timestamp in audio stream time_base
         * units
         */
        int64_t ImgNum2Pts(int img_num) {
            return img_num * timestamp_step;
        }

        /**
         * @brief Calculate the constant that converts an image number into a
         * timestamp with the stream's time base.
         *
         */
        void set_timestamp_step() {
            timestamp_step = static_cast<int64_t>(input->time_step
                / av_q2d(
                    input->format_context->streams[stream_index]->time_base));
        }

        private:
        int64_t timestamp_step;
    };

    struct Video : public Stream {
        cv::VideoCapture capture;
        std::thread process_thread;
        BlockingQueue<cv::Mat> frames{ AV_INPUT_VIDEO_FRAMES_QUEUE_SIZE };
        BlockingQueue<cv::Mat> images{ AV_INPUT_VIDEO_IMAGES_QUEUE_SIZE };
        // Vector of processor functions that can modify an image.
        std::vector<std::function<cv::Mat(cv::Mat*)>> processors{
            [](cv::Mat* image) { return image->clone(); },
            process::video::blur,
        };
        int processor_index             = 0;
        std::atomic<bool> is_processing = false;

        /**
         * @brief Construct a new Video object initializing the opencv capture
         * object and time_step constant.
         *
         * @param parent Parent audiovisual input, allows access to the packet,
         * the format context, etc.
         * @param filename Path to the video file to be loaded.
         */
        Video(AV_input* parent, const std::string& filename)
        : Stream(parent, AVMEDIA_TYPE_VIDEO) {
            capture          = cv::VideoCapture(filename);
            input->time_step = 1 / capture.get(cv::CAP_PROP_FPS);
            set_timestamp_step();
        };

        /**
         * @brief Construct a new empty Video object.
         *
         */
        Video(){};

        /**
         * @brief Producer for images. Consumer of frames. Processes the video
         * data to generate valid images.
         *
         */
        static void ProcessFrames(AV_input* input);
    };

    struct Audio : public Stream {
        std::thread process_thread;
        BlockingQueue<AVFrame*> frames{ AV_INPUT_AUDIO_FRAMES_QUEUE_SIZE };
        BlockingQueue<cv::Mat> images{ AV_INPUT_AUDIO_IMAGES_QUEUE_SIZE };
        // Vector of processor functions that can transform a vector of audio
        // data obtained from the audio frames into an image.
        std::vector<std::function<cv::Mat(std::vector<uint8_t>* data)>>
            processors;
        int processor_index             = 0;
        std::atomic<bool> is_processing = false;
        SwrContext* resample_context    = NULL;

        // TODO move to cpp
        /**
         * @brief Construct a new Audio object and open the stream.
         *
         * @param parent Parent audiovisual input, allows access to the packet,
         * the format context, etc.
         */
        Audio(AV_input* parent) : Stream(parent, AVMEDIA_TYPE_AUDIO) {
            // Open the audio stream
            int error = avcodec_open2(codec_context, codec, NULL);
            if (error < 0) {
                throw std::runtime_error("Cannot open the audio codec");
            }
            int sample_rate = codec_context->sample_rate;
            processors      = { process::audio::signal,
                process::audio::mfcc_factory(sample_rate) };
            set_timestamp_step();
            // Create resampler context
            resample_context = swr_alloc_set_opts(resample_context,
                // Output: channel layout, sample format, sample rate
                av_get_default_channel_layout(codec_context->channels),
                AV_SAMPLE_FMT_U8, sample_rate,
                // Input: channel layout, sample format, sample rate, etc.
                av_get_default_channel_layout(codec_context->channels),
                codec_context->sample_fmt, sample_rate, 0, NULL);
            if (resample_context == NULL) {
                throw std::runtime_error("Cannot allocate resampler context");
            }
            if (swr_init(resample_context) < 0) {
                throw std::runtime_error("Cannot initialize resampler context");
            }
        };

        Audio(){};

        ~Audio() {
            if (resample_context != NULL)
                swr_close(resample_context);
        }

        /**
         * @brief Producer for images. Consumer of frames. Processes the audio
         * data to generate valid images.
         *
         */
        static void ProcessFrames(AV_input* input);

        /**
         * @brief Compute the image number in the back of the images queue by
         * using the built in counter of pop() calls and the size of the queue.
         *
         * @return int Image number of the next image pushed to the images
         * queue.
         */
        int back_image_num() {
            return images.pop_count + images.size();
        };
    };

    public:
    Video* video;
    Audio* audio;
    double time_step; // [seconds]
    std::thread read_thread;
    std::atomic<bool> is_reading    = false;
    std::atomic<bool> is_stopping   = false;
    AVFormatContext* format_context = NULL;
    AVFrame* frame                  = av_frame_alloc();

    /**
     * @brief Construct a new av input object and load a video file.
     *
     * @param filename Path to the video file to be loaded.
     */
    AV_input(const std::string& filename) {
        if (!filename.empty()) {
            Load(filename);
        }
    }

    /**
     * @brief Construct a new empty audiovisual input object.
     *
     */
    AV_input() : AV_input(""){};

    // TODO add optional argument of audio file only

    /**
     * @brief Loads audio and video data from a video file.
     *
     */
    void Load(const std::string& filename);


    /**
     * @brief Destroy the audiovisual input object. Closes the
     * AVFormatContext.
     *
     */
    ~AV_input();

    /**
     * @brief Wrapper for av_read_frame. Reads the next frame.
     *
     * @return 1 if a frame was read, 0 if no frame was read, -1 if an error
     */
    int ReadFrame() {
        return av_read_frame(format_context, packet);
    }

    /**
     * @brief Wrapper for av_seek_frame and opencv set. Seeks to a given image
     * number.
     *
     * @param img_num
     * @return int >= 0 if seek was successful, < 0 if an error occurred
     */
    int SeekFrame(int img_num);

    /**
     * @brief Producer for audio_queue and video_queue. Extracts audio and
     * video data from the loaded audiovisual input.
     *
     * @param input Audiovisual input object
     */
    static void Read(AV_input* input);

    /**
     * @brief Initialize the image production pipeline. Stops the pipeline if
     * it was running before.
     *
     * @param int img_num: Image number to start the pipeline from.
     */
    void StartReading(int img_num = 0);

    /**
     * @brief Stops the image production pipeline. Signals to stop the reading
     * and processing threads and waits for them to finish. Then empties the
     * queues.
     *
     */
    void StopReading();

    static void ConsumeImages(AV_input* input);

    /**
     * @brief Checks if packet.stream_index is video stream index.
     *
     * @return bool
     */
    bool current_packet_is_video() const {
        return packet->stream_index == video->stream_index;
    }

    /**
     * @brief Checks if packet.stream_index is audio stream index.
     *
     * @return bool
     */
    bool current_packet_is_audio() const {
        return packet->stream_index == audio->stream_index;
    }

    /**
     * @brief Extracts the video and audio images from its respective queues.
     *
     * @return std::tuple<cv::Mat, cv::Mat> Video and audio images,
     * respectively.
     */
    std::tuple<cv::Mat, cv::Mat> get_images();

    private:
    AVPacket* packet = av_packet_alloc();
};
#endif // AV_INPUT_HPP_INCLUDED