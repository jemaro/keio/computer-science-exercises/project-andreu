
#ifndef BLOCKING_QUEUE_HPP_INCLUDED
#define BLOCKING_QUEUE_HPP_INCLUDED

#include <algorithm>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>

template <typename T> class BlockingQueue {
    private:
    size_t _capacity;
    std::queue<T> _queue;
    std::mutex _mutex;
    std::condition_variable _not_full;
    std::condition_variable _not_empty;

    public:
    int pop_count = 0; // Increased in every pop() call

    BlockingQueue(size_t capacity) : _capacity(capacity) {
        // empty
    }

    size_t size() {
        std::unique_lock<std::mutex> lock(_mutex);
        return _queue.size();
    }

    bool empty() {
        std::unique_lock<std::mutex> lock(_mutex);
        return _queue.empty();
    }

    void push(const T& elem) {
        {
            std::unique_lock<std::mutex> lock(_mutex);

            // wait while the queue is full
            while (_queue.size() >= _capacity) {
                _not_full.wait(lock);
            }
            _queue.push(elem);
        }
        _not_empty.notify_all();
    }

    void pop() {
        {
            std::unique_lock<std::mutex> lock(_mutex);

            // wait while the queue is empty
            while (_queue.size() == 0) {
                _not_empty.wait(lock);
            }
            _queue.pop();
            ++pop_count;
        }
        _not_full.notify_one();
    }

    const T& front() {
        std::unique_lock<std::mutex> lock(_mutex);

        // wait while the queue is empty
        while (_queue.size() == 0) {
            _not_empty.wait(lock);
        }
        return _queue.front();
    }

    void clear() {
        std::unique_lock<std::mutex> lock(_mutex);

        // Pop until the queue is empty
        while (_queue.size() > 0) {
            _queue.pop();
            _not_full.notify_one();
        }
        pop_count = 0;
    }
};

#endif // BLOCKING_QUEUE_HPP_INCLUDED