#ifndef PROCESSORS_HPP_INCLUDED
#define PROCESSORS_HPP_INCLUDED

#include <aquila.h>
#include <opencv2/imgproc/imgproc.hpp>

namespace process {

namespace audio {

static cv::Mat plot(uint8_t* data,
    int data_size,
    int height                         = 255,
    const cv::Scalar& line_color       = cv::Scalar(0, 0, 255),
    int line_thickness                 = 2,
    int line_style                     = 4,
    int type                           = CV_8UC3,
    const cv::Scalar& background_color = cv::Scalar(0, 0, 0)) {
    cv::Mat mat = cv::Mat(height, data_size, type, background_color);
    for (int k = 0; k < data_size - 1; ++k) {
        cv::line(mat, cv::Point(k, data[k]), cv::Point(k + 1, data[k + 1]),
            line_color, line_thickness, line_style, 0);
    }
    cv::resize(mat, mat, cv::Size(640, 480));
    return mat;
};

static cv::Mat plot(std::vector<uint8_t>* data,
    int height                         = 255,
    const cv::Scalar& line_color       = cv::Scalar(0, 0, 255),
    int line_thickness                 = 2,
    int line_style                     = 4,
    int type                           = CV_8UC3,
    const cv::Scalar& background_color = cv::Scalar(0, 0, 0)) {
    return plot(data->data(), data->size(), height, line_color, line_thickness,
        line_style, type, background_color);
};

static cv::Mat signal(std::vector<uint8_t>* data) {
    return plot(data);
};

static cv::Mat
mfcc(std::vector<uint8_t>* data, int sample_rate = 48000, int num_coeffs = 24) {
    Aquila::SignalSource signal_source(data->data(), data->size(), sample_rate);
    Aquila::Mfcc mfcc(data->size());
    std::vector<double> mfcc_data = mfcc.calculate(signal_source, num_coeffs);
    // Normalize
    double a = *std::min_element(mfcc_data.begin(), mfcc_data.end());
    double b = *std::max_element(mfcc_data.begin(), mfcc_data.end());
    a /= b;
    std::transform(mfcc_data.begin(), mfcc_data.end(), mfcc_data.begin(),
        [a, b](double x) { return (x / b - a) * 255; });
    cv::Mat audio_img(mfcc_data);
    audio_img.convertTo(audio_img, CV_8UC1);
    cv::resize(audio_img.t(), audio_img, cv::Size(320, 240));
    cv::applyColorMap(audio_img, audio_img, cv::COLORMAP_JET);
    return audio_img;
};

static std::function<cv::Mat(std::vector<uint8_t>*)>
mfcc_factory(int sample_rate, int num_coeffs = 24) {
    return [sample_rate, num_coeffs](std::vector<uint8_t>* data) {
        return process::audio::mfcc(data, sample_rate, num_coeffs);
    };
};

}; // namespace audio

namespace video {

static cv::Mat blur(cv::Mat* mat) {
    cv::Mat blurred;
    cv::blur(*mat, blurred, cv::Size(10, 10));
    return blurred;
};

}; // namespace video

}; // namespace process

#endif // PROCESSORS_HPP_INCLUDED