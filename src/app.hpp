#ifndef APP_HPP_INCLUDED
#define APP_HPP_INCLUDED

#include <FL/Fl_Native_File_Chooser.H>
#ifdef DEBUG
#include <iostream>
#endif

#include "AV_input.hpp"
#include "appUI.hpp"
#include "blocking_queue.hpp"

/**
 * @brief Application class where all the application data is contained.
 *
 */
class App {

    class DataPipeline {

        private:
    };

    public:
    short int playback_speed = 1;          // Divides the time between frames
    AV_input input           = AV_input(); // Audiovisual input
    AppUI ui                 = AppUI();    // User interface

    // TODO Queue of audio data that needs to sound

    /**
     * @brief Construct a new App object and execute the Load function with the
     * given filename.
     *
     * @param filename Path to the video file to be loaded.
     */
    App(const std::string& filename);

    /**
     * @brief Construct a new App object without loading any file.
     *
     */
    App() : App(""){};

    /**
     * @brief Sets the playback status to a given value.
     *
     * @param value Desired playback status.
     */
    void Play(bool value) {
        ui.play_button->value(value);
        ui.play_button->do_callback();
    }

    /**
     * @brief Toggle play/pause.
     *
     */
    void Play() {
        Play(!ui.play_button->value());
    };

    /**
     * @brief Loads audio and video data from a video file.
     *
     * @param filename Path to the video file.
     */
    void Load(const std::string& filename) {
        input.Load(filename);
        _Load();
    }

    // TODO
    // void Load(
    //     const std::string& video_filename,
    //     const std::string& audio_filename
    // );

    /**
     * @brief Goes to the specified frame.
     *
     * @param frame
     */
    void GoToFrame(int frame_number) {
        std::cout << "Going to frame " << frame_number << std::endl;
        input.StartReading(frame_number);
        UpdateViewer(this);
    }

    // Accessors and mutators

    /**
     * @brief Wether if the file is playing or not.
     *
     * @return int 1 if playing, 0 if not.
     */
    int is_playing() {
        return ui.play_button->value();
    };

    /**
     * @brief Get the time between frames in seconds.
     *
     * @return double Time between frames in seconds.
     */
    double get_time_step() {
        return input.time_step / playback_speed;
    };

    int current_frame_number() {
        if (input.video->images.pop_count != input.audio->images.pop_count) {
            throw std::runtime_error("Images are not being displayed in sync");
        }
        return input.video->images.pop_count - 1;
    };


    private:
    /**
     * @brief Common load method. Initialize threads with the loaded
     * audiovisual input
     *
     */
    void _Load();

    // Callbacks

    /**
     * @brief Callback for the Load menu item. Shows a file chooser and loads
     * the selected file using the Load function.
     *
     * @param userdata Pointer to an App object.
     */
    static void LoadCallback(Fl_Widget*, void* userdata);

    /**
     * @brief Callback for the play button. Updates the button label and adds
     * UpdateViewer as a timeout function to the UI if it is not already added.
     *
     * @param widget Fl_Button object.
     * @param userdata Pointer to an App object.
     */
    static void PlayButtonCallback(Fl_Widget* widget, void* userdata);

    /**
     * @brief Callback for the speed change button. Updates the button label
     * and the playback_speed attribute.
     *
     * @param widget
     * @param userdata Pointer to an App object.
     */
    static void SpeedButtonCallback(Fl_Widget* widget, void* userdata);

    /**
     * @brief Callback for the forward button. Skips the next frame.
     *
     * @param userdata Pointer to an App object.
     */
    static void ForwardButtonCallback(Fl_Widget*, void* userdata);

    /**
     * @brief Callback for the backward button. Goes to the previous frame.
     *
     * @param userdata Pointer to an App object.
     */
    static void BackwardButtonCallback(Fl_Widget*, void* userdata);

    /**
     * @brief Callback for the frame slider. Updates the next_frame attribute
     * and seeks the frame in the audiovisual input.
     *
     * @param userdata Pointer to an App object.
     */
    static void FrameSliderCallback(Fl_Widget*, void* userdata);

    /**
     * @brief Callback for the video processor selector. Updates the processor
     * index in the audiovisual input.
     * 
     * @param widget FL_Choice object.
     * @param userdata Pointer to an App object.
     */
    static void VideoChoiceCallback(Fl_Widget* widget, void* userdata);

    /**
     * @brief Callback for the video processor selector. Updates the processor
     * index in the audiovisual input.
     * 
     * @param widget FL_Choice object.
     * @param userdata Pointer to an App object.
     */
    static void AudioChoiceCallback(Fl_Widget* widget, void* userdata);

    // Displayer thread

    /**
     * @brief Consumer of audio_img_queue and video_img_queue. Updates the User
     * Interface video and audio viewers. Therefore it is necessary to have it
     * completely defined inside the App class.
     *
     * @param userdata Pointer to an App object.
     */
    static void UpdateViewer(void* userdata);
};

#endif // APP_HPP_INCLUDED