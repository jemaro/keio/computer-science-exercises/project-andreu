/*
Based on Fl_ViewerCV.h programmed by Vincent Crocher
https://sourceforge.net/p/bdreader/code/ci/master/tree/src/
https://sourceforge.net/u/kennyyy/profile/
*/

/**
 * \file Fl_ViewerCV.h
 * \brief Declaration file of the Fl_ViewerCV class
 * \author Andreu Gimenez
 * \date November 2021
 */

#ifndef Fl_ViewerCV_H
#define Fl_ViewerCV_H

#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/fl_ask.H>
#include <FL/fl_draw.H>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

cv::Mat* fl_LoadImage(char* filename, int iscolor);
bool fl_SaveImage(char* filename, cv::Mat* image);

// Convert the fltk mouse event into opencv ones (to be able to use open cv
// callback functions and events)
int fl_event2cv_event(int fl_event);


// A widget displaying an image in OpenCv format (or a portion of it) and
// managing user interaction.
class FL_EXPORT Fl_ViewerCV : public Fl_Widget {
    public:
    Fl_ViewerCV(int X, int Y, float mzoom = 1.);
    Fl_ViewerCV(int X, int Y, int W, int H, float mzoom = 1.);
    ~Fl_ViewerCV();

    // Assign a new image to draw (in cv::Mat Opencv format)
    void SetImage(cv::Mat* nimage, cv::Rect disprect = cv::Rect(0, 0, 0, 0));
    // Assign a new portion (rectangle) of the image to draw (in cv::Rect
    // Opencv format)
    void SetDisplayRect(cv::Rect disprect);
    void UpdateView();

    // TODO void PlayVideoCapture(cv::VideoCapture*, callback)


    int handle(int event);
    void resize(int x, int y, int ww, int hh);

    // void SetHandleKeys(bool val){HandleKeys=val;}
    // void SetCVMouseCallback(CvMouseCallback on_mouse, void * param=0)
    // {
    //   mouse_cb=on_mouse;
    //   CVcb_param=param;
    // }

    friend void ResizingTimer_cb(void* flopencv);
    void SetResizing_cb(void (*cb)(Fl_Widget*, void*), void* param = NULL) {
        Resizing_cb       = cb;
        Resizing_cb_param = param;
    }

    void SetOnClick_cb(void (*cb)(Fl_Widget*, void*), void* param = NULL) {
        OnClick_cb       = cb;
        OnClick_cb_param = param;
    }

    void SetOnRelease_cb(void (*cb)(Fl_Widget*, void*), void* param = NULL) {
        OnRelease_cb       = cb;
        OnRelease_cb_param = param;
    }


    bool IsInContext() {
        return InContext;
    }
    void SetInContext(bool val) {
        InContext = val;
    }
    bool IsBorder() {
        return Border;
    }
    void SetBorder(bool val) {
        Border = val;
    }
    float GetScalingFactor() {
        return Scale;
    }
    void SetMaxZoom(float val) {
        MaxZoom = val;
    }
    float GetMaxZoom() {
        return MaxZoom;
    }
    void SetCustomZoom(float val) {
        fmin(fmax(CustomZoom = val, 0.8), 2.0);
    }
    float GetCustomZoom() {
        return CustomZoom;
    }
    float GetCurrentZoom() {
        return 1. / Scale;
    }


    protected:
    // Draw the image
    void draw(int, int, int, int);
    void draw() {
        draw(Fl_Widget::x(), Fl_Widget::y(), w(), h());
    }

    // cv::Mat fimage;
    int iscolor;
    bool fit;
    bool InContext;
    bool Border;
    float Scale;
    float MaxZoom;
    float CustomZoom;
    bool RmbInContext;
    float RmbCustomZoom;
    bool ShowPreview;
    bool Magnifier;

    // Callbacks
    bool HandleKeys;
    void (*Resizing_cb)(Fl_Widget*, void*);
    void* Resizing_cb_param;
    void (*OnClick_cb)(Fl_Widget*, void*);
    void* OnClick_cb_param;
    void (*OnRelease_cb)(Fl_Widget*, void*);
    void* OnRelease_cb_param;
    // CvMouseCallback mouse_cb;
    void* CVcb_param;
    bool Resizing;

    cv::Rect DisplayRect;
    cv::Mat *image, fimage;
    Fl_RGB_Image* FlImg;
};

#endif
