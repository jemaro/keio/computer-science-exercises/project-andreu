#include <iostream>

#include "app.hpp"

// Constructor and Destructor

App::App(const std::string& filename) {
    // Add processor options
    ui.video_choice->add("Raw|Blur");
    ui.audio_choice->add("Signal|MFCC");
    ui.video_choice->value(0);
    ui.audio_choice->value(0);
    // Assign callbacks
    ui.load_video_button->callback(LoadCallback, this);
    ui.play_button->callback(PlayButtonCallback, this);
    ui.speed_button->callback(SpeedButtonCallback, this);
    ui.forward_button->callback(ForwardButtonCallback, this);
    ui.backward_button->callback(BackwardButtonCallback, this);
    ui.frame_slider->callback(FrameSliderCallback, this);
    ui.video_choice->callback(VideoChoiceCallback, this);
    ui.audio_choice->callback(AudioChoiceCallback, this);
    // Show the window
    ui.main_window->show();
    // Load file if given
    if (!filename.empty()) {
        Load(filename);
    }
}

// Methods

void App::_Load() {
    // Initialize the image production pipeline
    input.StartReading();
    // Update slider length
    ui.frame_slider->maximum(
        input.video->capture.get(cv::CAP_PROP_FRAME_COUNT) - 1);
    // Load the first frame
    UpdateViewer(this);
}

// Threads

void App::UpdateViewer(void* userdata) {
    App* app = static_cast<App*>(userdata);
    // Extract the images from the queues
    cv::Mat video_img, audio_img;
    std::tie(video_img, audio_img) = app->input.get_images();
    if (video_img.empty() && audio_img.empty()) { // End of file
        // Stop the playback
        app->Play(false);
    } else {
        // Display the images. Skip empty images
        if (!video_img.empty()) {
            app->ui.video_viewer->SetImage(&video_img);
            app->ui.video_viewer->UpdateView();
        }
        if (!audio_img.empty()) {
            app->ui.audio_viewer->SetImage(&audio_img);
            app->ui.audio_viewer->UpdateView();
        } 
        if (app->is_playing()) {
            Fl::repeat_timeout(app->get_time_step(), UpdateViewer, app);
        }
        // Update the slider
        app->ui.frame_slider->value(app->current_frame_number());
    }
}

// UI Callbacks

void App::LoadCallback(Fl_Widget*, void* userdata) {
    App* _this = static_cast<App*>(userdata);
    // Create native chooser
    Fl_Native_File_Chooser native;
    native.title("Pick a file");
    native.type(Fl_Native_File_Chooser::BROWSE_FILE);
    native.filter("*.mp4");
    // Show native chooser
    switch (native.show()) {
    case -1: fprintf(stderr, "ERROR: %s\n", native.errmsg()); break; // ERROR
    case 1:
        fprintf(stderr, "*** CANCEL\n");
        fl_beep();
        break; // CANCEL
    default:   // PICKED FILE
        if (native.filename()) {
            std::string filename(native.filename());
            _this->Load(filename);
        }
        break;
    }
}

void App::PlayButtonCallback(Fl_Widget* widget, void* userdata) {
    std::cout << "PlayButtonCallback" << std::endl;
    Fl_Button* play_button = static_cast<Fl_Button*>(widget);
    App* _this             = static_cast<App*>(userdata);
    if (play_button->value()) { // Play
        if (!_this->input.is_reading.load()) {
            _this->input.StartReading();
        }
        Fl::add_timeout(_this->get_time_step(), UpdateViewer, _this);
        play_button->copy_label("@||");
    } else { // Stop
        play_button->copy_label("@>");
    }
}

void App::SpeedButtonCallback(Fl_Widget* widget, void* userdata) {
    App* _this              = static_cast<App*>(userdata);
    Fl_Button* speed_button = static_cast<Fl_Button*>(widget);
    _this->playback_speed   = (_this->playback_speed * 2) % 31;
    std::cout << _this->playback_speed << std::endl;
    speed_button->copy_label(
        ("x" + std::to_string(_this->playback_speed)).c_str());
}

void App::ForwardButtonCallback(Fl_Widget*, void* userdata) {
    App* _this = static_cast<App*>(userdata);
    _this->Play(false);
    UpdateViewer(_this);
}

void App::BackwardButtonCallback(Fl_Widget*, void* userdata) {
    std::cout << "BackwardButtonCallback" << std::endl;
    App* _this = static_cast<App*>(userdata);
    _this->Play(false);
    _this->GoToFrame(_this->current_frame_number() - 1);
}

void App::FrameSliderCallback(Fl_Widget* widget, void* userdata) {
    std::cout << "FrameSliderCallback" << std::endl;
    Fl_Value_Slider* frame_slider = static_cast<Fl_Value_Slider*>(widget);
    App* _this                    = static_cast<App*>(userdata);
    _this->Play(false);
    _this->GoToFrame(frame_slider->value());
}

void App::AudioChoiceCallback(Fl_Widget* widget, void* userdata) {
    Fl_Choice* choice = static_cast<Fl_Choice*>(widget);
    App* _this        = static_cast<App*>(userdata);
    _this->input.audio->processor_index = choice->value();
}

void App::VideoChoiceCallback(Fl_Widget* widget, void* userdata) {
    Fl_Choice* choice = static_cast<Fl_Choice*>(widget);
    App* _this        = static_cast<App*>(userdata);
    _this->input.video->processor_index = choice->value();
}

// Main loop

int main(int argc, char* argv[]) {
    App* app;
    if (argc < 2) {
        app = new App();
    } else if (argc < 3) {
        app = new App(std::string(argv[1]));
    } else {
        std::cout << "Usage: " << argv[0] << " <filename>" << std::endl;
        return EXIT_FAILURE;
    }
    std::cout << "Run" << std::endl;
    // This will block until the user closes the window
    int result = Fl::run();
    std::cout << "Exit" << std::endl;
    delete app;
    return result;
}