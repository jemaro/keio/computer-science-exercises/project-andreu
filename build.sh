#!/bin/bash
cd "$(dirname "$0")"

cd 3rdparty/aquila
mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-std=c++11 -fPIC"
make -j

cd "$(dirname "$0")"

mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make -j
